﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Daily_Expense
{
    public partial class Form1 : Form
    {
        SqlConnection sqlCon = new SqlConnection(@"Data Source=LAPTOP-JQK15P7R;Initial Catalog=DB1;User ID=sa;Password=core044#");//Integrated Security=True");
       // String str = @"Data Source=LAPTOP-JQK15P7R;database=DB1;UID=sa;password=core044#";
        //SqlConnection sqlCon = new SqlConnection(str);
        int item_no = 0;
        int i = 0;
       
        public Form1()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (sqlCon.State == ConnectionState.Closed)
                {
                    sqlCon.Open();
                }
                if (btnSave.Text == "Save")
                {

                    SqlCommand sqlCmd = new SqlCommand("ItemAddOrEdit", sqlCon);
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.Parameters.AddWithValue("@mode", "Add");
                    sqlCmd.Parameters.AddWithValue("@Item", textBox1.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@Price", textBox3.Text);
                    sqlCmd.Parameters.AddWithValue("@Added_By", textBox2.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@Created_ON", dateTimePicker1.Text);
                    sqlCmd.Parameters.AddWithValue("@Item_no", 0);
                    sqlCmd.ExecuteNonQuery();
                    textBox1.Clear();
                    textBox2.Clear();
                    textBox3.Clear();
                    textBox4.Clear();
                    MessageBox.Show("Saved Successfully");
                  }

                else
                {
                    SqlCommand sqlCmd = new SqlCommand("ItemAddOrEdit", sqlCon);
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.Parameters.AddWithValue("@mode", "Edit");
                    sqlCmd.Parameters.AddWithValue("@Item", textBox1.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@Price", textBox3.Text);
                    sqlCmd.Parameters.AddWithValue("@Added_By", textBox2.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@Created_ON", dateTimePicker2.Text);
                    sqlCmd.Parameters.AddWithValue("@Item_no", item_no);
                    sqlCmd.ExecuteNonQuery();
                    textBox1.Clear();
                    textBox2.Clear();
                    textBox3.Clear();
                    textBox4.Clear();
                    MessageBox.Show("Updated Successfully");
                    
                }
                FildataGridView();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error message");
                throw;
            }

            finally
            {
                sqlCon.Close();
            }
        }

        private void FildataGridView()
        {
            if (sqlCon.State == ConnectionState.Closed)
                sqlCon.Open();
            if (i != 2)
            {
                SqlDataAdapter sqlData = new SqlDataAdapter("ItemSearch", sqlCon);
                sqlData.SelectCommand.CommandType = CommandType.StoredProcedure;
                sqlData.SelectCommand.Parameters.AddWithValue("@Created_ON", txtSearch.Text);
                DataTable dtb1 = new DataTable();
                sqlData.Fill(dtb1);
                dgvItems.DataSource = dtb1;
            }
            else
            {

                SqlDataAdapter sqlData = new SqlDataAdapter("ItemSearch_All", sqlCon);
                sqlData.SelectCommand.CommandType = CommandType.StoredProcedure;
                DataTable dtb1 = new DataTable();
                sqlData.Fill(dtb1);
                dgvItems.DataSource = dtb1;
            }

            //txtSearch.Clear();
            sqlCon.Close();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            textBox4.Text = dateTimePicker1.Text;
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            txtSearch.Text = dateTimePicker2.Text;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                i = 1;
                FildataGridView();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error message");
            }
        }

        private void dgvItems_DoubleClick(object sender, EventArgs e)
        {
            if (dgvItems.CurrentRow.Index != -1)
            {
                item_no = Convert.ToInt32(dgvItems.CurrentRow.Cells[4].Value.ToString());
                textBox1.Text = dgvItems.CurrentRow.Cells[0].Value.ToString();
                textBox3.Text = dgvItems.CurrentRow.Cells[1].Value.ToString();
                textBox2.Text = dgvItems.CurrentRow.Cells[2].Value.ToString();
                textBox4.Text = dgvItems.CurrentRow.Cells[3].Value.ToString();
                btnSave.Text = "Update";
                btnDelete.Enabled = true;
            }

        }

        void Reset()
        {
            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();
            textBox4.Clear();
            txtSearch.Clear();
            btnSave.Text = "Save";
            item_no = 0;
            i = 0;
            btnDelete.Enabled = false;
            FildataGridView();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            Reset();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (sqlCon.State == ConnectionState.Closed)
                sqlCon.Open();
            SqlCommand sqlCmd = new SqlCommand("ItemDelete", sqlCon);
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.Parameters.AddWithValue("@Item_no", item_no);
            sqlCmd.ExecuteNonQuery();
            MessageBox.Show("Deleted Successfully");
            FildataGridView();
            sqlCon.Close();
        }

        private void btnsearchall_Click(object sender, EventArgs e)
        {
            try
            {
                i = 2;
                FildataGridView();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error message");
            }
        }
    }
}
